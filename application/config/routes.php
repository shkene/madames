<?php
defined('BASEPATH') OR exit('No direct script access allowed');

$route['clubs'] = 'clubs/index';
$route['girls/update_bio'] = 'girls/update_bio';
$route['girls/update_services'] = 'girls/update_services';
$route['girls/register'] = 'girls/register';
$route['girls/login'] = 'girls/login';
$route['girls/logout'] = 'girls/logout';
$route['girls/(:any)'] = 'girls/view_profile/$1';
$route['girls'] = 'girls/index';
$route['default_controller'] = 'pages/view';
$route ['girls/regions/(:any)'] = 'girls/regions/$1';
$route ['girls/regions_new/(:any)'] = 'girls/regions_new/$1';
$route['(:any)'] = 'pages/view/$1';
$route['404_override'] = '';
$route['translate_uri_dashes'] = FALSE;
