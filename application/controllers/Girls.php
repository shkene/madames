<?php
   class Girls extends CI_Controller{
     public function view_profile($slug = NULL){
        $data['girl'] = $this->girl_model->get_girl_by_slug($slug);
        $data['services'] = $this->girl_model->get_services_for_girl($slug);
        if(empty($data['girl'])){
            show_404();
          }
          $user_nickname = $data['girl']->username;
          $this->load->view('templates/header');
          $this->load->view('girls/profile', $data);
          $this->load->view('templates/footer');
     }
     public function register(){
       $data['title'] = 'Register';

       $data['regions'] = $this->girl_model->get_regions();

      $this->form_validation->set_rules('username', 'Username','required|callback_check_username_exists');
      $this->form_validation->set_rules('password', 'Password','required');
      $this->form_validation->set_rules('passwordtwo', 'Confirm Password','matches[password]');
      $this->form_validation->set_rules('email', 'Email','required|callback_check_email_exists');
      $this->form_validation->set_rules('region_id', 'Region_id','required');

      if($this->form_validation->run() === FALSE){
        $this->load->view('templates/header');
        $this->load->view('girls/register', $data);
        $this->load->view('templates/footer');
      }else{
        //Encrypt password
        $enc_password = md5($this->input->post('password'));

        //Upload profile photo
        $config['upload_path'] = './assets/images/girls';
        $config['allowed_types'] = 'jpg|png';
        $config['max_size'] = '2048';
        $config['max_width'] = '500';
        $config['max_height'] = '500';

        $this->load->library('upload', $config);

        if(!$this->upload->do_upload()){
            $errors = array('error' => $this->upload->display_errors());
            $profile_photo= 'noimage.jpg';
        }else{
            $data = array('upload_data' => $this->upload->data());
            $profile_photo = $_FILES['userfile']['name'];
        }

        $this->girl_model->register($profile_photo);

        //Set message
        $this->session->set_flashdata('user_registered', 'You are now registered and can log in');

        redirect('/');
      }
     }
     public function login(){
       $data['title'] = 'Login';

      $this->form_validation->set_rules('username', 'Username','required');
      $this->form_validation->set_rules('password', 'Password','required');

      if($this->form_validation->run() === FALSE){
        $this->load->view('templates/header');
        $this->load->view('girls/login', $data);
        $this->load->view('templates/footer');
      }else{
        //Get username
        $username = $this->input->post('username');
        //Get and encrypt password
        $password = $this->input->post('password');
        //Login user
        $user_id = $this->girl_model->login($username, $password);

        $girl = $this->girl_model->get_logged_girl($user_id);
        if($user_id){
          //create session
          $user_data = array(
            'user_id' => $user_id,
            'username' => $username,
            'logged_in' => true,
          );


          $this->session->set_userdata($user_data);
          //Set message
          $this->session->set_flashdata('user_loggedin', 'Welcome! You are now logged in');

          redirect('/');
        }else{
          //Set message
          $this->session->set_flashdata('login_failed', 'Login is invalid. Try again with correct username or password');

          redirect('girls/login');
        }


      }
     }
     public function regions($id){
         $data['girls'] = $this->girl_model->get_girls_by_region($id);
         $data['new_girls'] = $this->girl_model->get_new_girls_by_region($id);
         $data['regions'] = $this->girl_model->get_regions();
         $this->load->view('templates/header');
         $this->load->view('pages/home', $data);
         $this->load->view('templates/footer');
     }

     public function regions_new($id){
         $data['new_girls'] = $this->girl_model->get_new_girls_by_region($id);
         $data['regions'] = $this->girl_model->get_regions();
         $this->load->view('templates/header');
         $this->load->view('pages/new-girls', $data);
         $this->load->view('templates/footer');
     }
     // Check username exists
    public function check_username_exists($username){
         $this->form_validation->set_message('check_username_exists','That username is taken. Please choose another username');
         if($this->girl_model->check_username_exists($username)){
           return true;
         }else{
           return false;
         }
     }
     //Log user out
     public function logout(){
       //Unset user data
       $this->session->unset_userdata('logged_in');
       $this->session->unset_userdata('user_id');
       $this->session->unset_userdata('username');

       //Set message
       $this->session->set_flashdata('user_loggedout', 'You are now logged out');

       redirect('girls/login');
     }

     // Check email exists
     public function check_email_exists($email){
         $this->form_validation->set_message('check_email_exists','That email is taken. Please choose another username');
         if($this->girl_model->check_email_exists($email)){
           return true;
         }else{
           return false;
         }
     }

     public function phone_number_format(){
       $data['girls'] = $this->girl_model->get_girls();
       $contact = $girls['contact'];
     }

     public function bio($slug){
       $data['girl'] = $this->girl_model->get_girl_by_slug($slug);
       $data['regions'] = $this->girl_model->get_regions();

       if(empty($data)){
           show_404();
         }
         $user_nickname = $data['girl']->username;
         $this->load->view('templates/header');
         $this->load->view('girls/bio', $data);
         $this->load->view('templates/footer');
     }

     public function update_bio($slug){
       $this->girl_model->update_bio();
       redirect('girls/view_profile/'.$slug);
     }

     public function update_services($slug){
       $this->girl_model->update_service();
       redirect('girls/view_profile/'.$slug);
     }

     public function services($slug){
       $data['girl'] = $this->girl_model->get_girl_by_slug($slug);
       $data['regions'] = $this->girl_model->get_regions();
       $data['services'] = $this->girl_model->get_services();

       if(empty($data)){
           show_404();
         }
         $user_nickname = $data['girl']->username;
         $this->load->view('templates/header');
         $this->load->view('girls/services', $data);
         $this->load->view('templates/footer');
     }
   }



?>
