<?php
   class Clubs extends CI_Controller{
     public function index(){
        $data['title'] = 'Clubs';

        $data['clubs'] = $this->club_model->get_clubs();
        $data['regions'] = $this->girl_model->get_regions();

        $this->load->view('templates/header');
        $this->load->view('clubs/index', $data);
        $this->load->view('templates/footer');
     }


     // public function profile($user_nickname = NULL){
     //   $data['girl'] = $this->girl_model->get_girls($user_nickname);
     //
     //   $data['title'] = $data['girl']['user_nickname'];
     //
     //   $this->load->view('templates/header');
     //   $this->load->view('pages/profile', $data);
     //   $this->load->view('templates/footer');
     // }
   }



?>
