<?php
   class Pages extends CI_Controller{
     public function view($page = 'home'){
        if(!file_exists(APPPATH.'views/pages/'.$page.'.php')){
          show_404();
        }

        $data['title'] = ucfirst($page);

        $data['girls'] = $this->girl_model->get_girls();
        $data['regions'] = $this->girl_model->get_regions();
        $data['new_girls'] = $this->girl_model->get_new_girls();

        $this->load->view('templates/header');
        $this->load->view('pages/'.$page, $data);
        $this->load->view('templates/footer');
     }


     // public function profile($user_nickname = NULL){
     //   $data['girl'] = $this->girl_model->get_girls($user_nickname);
     //
     //   $data['title'] = $data['girl']['user_nickname'];
     //
     //   $this->load->view('templates/header');
     //   $this->load->view('pages/profile', $data);
     //   $this->load->view('templates/footer');
     // }
   }



?>
