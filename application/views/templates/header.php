<html lang="en">

  <head>

    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>Madames.ch</title>

    <!-- Bootstrap core CSS -->
    <link href="<?php echo base_url(); ?>vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">
   <link href="https://fonts.googleapis.com/css?family=Droid+Sans:400,700" rel="stylesheet">
   <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/baguettebox.js/1.8.1/baguetteBox.min.css">
   <link rel="stylesheet" href="gallery-grid.css">


    <!-- Custom styles for this template -->
    <link href="<?php echo base_url(); ?>assets/css/shop-homepage.css" rel="stylesheet">

  </head>

  <body>

    <!-- Navigation -->
    <nav class="navbar navbar-expand-lg navbar-dark fixed-top">
      <div class="container">
        <a class="navbar-brand" href="/"><img src="<?php echo base_url(); ?>assets/images/logo1.png" class="logo"></a>
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarResponsive" aria-controls="navbarResponsive" aria-expanded="false" aria-label="Toggle navigation">
          <span class="navbar-toggler-icon"></span>
        </button>
        <div class="collapse navbar-collapse" id="navbarResponsive">
          <ul class="navbar-nav ml-auto">
            <li class="nav-item active">
              <a class="nav-link" href="<?php echo base_url(); ?>">All girls
                <span class="sr-only">(current)</span>
              </a>
            </li>
            <li class="nav-item">
              <a class="nav-link" href="<?php echo base_url(); ?>new-girls">New girls</a>
            </li>
            <li class="nav-item">
              <a class="nav-link" href="<?php echo base_url(); ?>clubs">Clubs</a>
            </li>
            <li class="nav-item">
              <a class="nav-link" href="<?php echo base_url(); ?>contacts">Contact</a>
            </li>
            <li style="border-left: 1px solid #999;"></li>
            <?php if(!$this->session->userdata('logged_in')) : ?>
            <li class="nav-item">
      					<a class="nav-link" href="<?php echo base_url(); ?>girls/login">Members</a>
          	</li>
            <?php endif; ?>
            <li class="nav-item">
              <a class="nav-link" href="#">Price</a>
            </li>
            <?php if($this->session->userdata('logged_in')) : ?>
            <li class="nav-item">
                            <a class="nav-link" href="<?php echo base_url(); ?>girls/view_profile/<?php echo $this->session->userdata('username');?>"><?php echo $this->session->userdata('username');?></a>
            </li>
            <li class="nav-item">
                  					<a class="nav-link" href="<?php echo base_url(); ?>girls/logout">Logout</a>
                            </li>

            <?php endif; ?>
          </ul>
        </div>
      </div>
    </nav>
