
    <!-- Page Content -->
    <div class="container container-holder">

      <div class="row">

        <div class="col-lg-3 aside-search">

          <h1 class="my-4">City/Regions</h1>
          <div class="list-group">
            <a class="list-group-item" href="#">Basel</a>
            <a class="list-group-item" href="#">Zurich</a>
            <a class="list-group-item" href="#">Bern</a>
            <a class="list-group-item" href="#">Geneva</a>
            <a class="list-group-item" href="#">St Gallen</a>
            <a class="list-group-item" href="#">Luzern</a>
            <a class="list-group-item" href="#">Lugano</a>
          </div>

        </div>
        <!-- /.col-lg-3 -->

        <div class="col-lg-9">

          <div class="row clubs_row">
            <?php foreach($clubs as $club) : ?>
            <div class="col-lg-3 col-md-6 mb-3">
              <div class="card">
                <div class="img-holder">
                    <span><?php echo $club['name']; ?></span>
                    <img class="card-img-top" src="<?php echo base_url(); ?>assets/images/girl.jpg" alt="">
                </div>
                <div class="card-body">
                  <h4 class="card-title">
                    <?php echo $club['club_name']; ?>
                  </h4>
                  <p class="address_club">

                  </p>
                </div>
             </div>
            </div>
          <?php endforeach; ?>
          </div>
          <!-- /.row -->

        </div>
        <!-- /.col-lg-9 -->

      </div>
      <!-- /.row -->

    </div>
    <!-- /.container -->
