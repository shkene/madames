
    <!-- Page Content -->
    <div class="container container-holder">

      <div class="row">

        <div class="col-lg-3 aside-search">

          <h1 class="my-4">City/Regions</h1>
          <div class="list-group">
            <?php foreach($regions as $region) : ?>
            <a class="list-group-item" href="<?php echo site_url('/girls/regions_new/'.$region['region_id']); ?>"><img class="sidebar_img" src="<?php echo base_url(); ?>assets/images/strelica1.png" alt=""><?php echo $region['name']; ?></a>
            <?php endforeach; ?>
          </div>

        </div>
        <!-- /.col-lg-3 -->

        <div class="col-lg-9 new_girls_wrapper">

          <div class="row top-filters">


          <div class="row">
            <?php foreach($new_girls as $new_girl) : ?>
            <div class="col-lg-11 col-md-8 mb-14 new-girl-holder">
              <div class="card new_girl_card">
                <div class="row">
                <div class="img-holder new-girls col-md-7">
                  <a href="#">
                    <img class="big-img" class="card-img-top" src="<?php echo base_url(); ?>assets/images/girl.jpg" alt="">
                    <img class="small-img small_img_first" class="card-img-top" src="<?php echo base_url(); ?>assets/images/girl.jpg" alt="">
                    <img class="small-img small_img_sec" class="card-img-top" src="<?php echo base_url(); ?>assets/images/girl.jpg" alt="">
                    <div class="cleaner"></div>
                  </a>
                </div>
                <div class="card-body col-md-5">
                  <h4 class="card-title new_girls_h4">
                    <a href="#"><?php echo $new_girl['username']; ?></a>
                  </h4>
                  <h6><b>City</b>: <?php echo $new_girl['name']; ?></h6>
                  <h6><b>Age</b>: <?php echo $new_girl['age']; ?></h6>
                  <h6><b>Phone</b>: <?php echo $new_girl['contact']; ?></h6>
                  <h6><b>Description</b>:</h6>
                </div>
                </div>
             </div>
            </div>
            <?php endforeach; ?>
          </div>
          <!-- /.row -->

        </div>
        <!-- /.col-lg-9 -->

      </div>
      <!-- /.row -->
    </div>
    </div>
    <!-- /.container -->
