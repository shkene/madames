<!-- Page Content -->
<div class="container container-holder">
  <?php echo validation_errors(); ?>
  <div class="login-form">
    <?php
     echo form_open('girls/login');
     ?>
        <h2 class="text-center">Sign In</h2>
        <div class="form-group">
        	<div class="input-group">
                <span class="input-group-addon"><i class="fa fa-user"></i></span>
                <input type="text" class="form-control" placeholder="Username" name="username" required="required">
            </div>
        </div>
		      <div class="form-group">
            <div class="input-group">
                <span class="input-group-addon"><i class="fa fa-lock"></i></span>
                <input type="password" class="form-control" placeholder="Password" name="password" required="required">
            </div>
        </div>
        <div class="form-group">
            <button type="submit" class="btn btn-primary btn-block">Log in</button>
        </div>
        <div class="clearfix">
            <a href="#" class="pull-right">Forgot Password?</a>
        </div>
    <?php echo form_close(); ?>
    <p class="text-center small">Don't have an account! <a href="<?php echo base_url(); ?>girls/register">Sign up here</a>.</p>
</div>
</div>
