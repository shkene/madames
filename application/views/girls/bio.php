
    <!-- Page Content -->
    <div class="container container-holder">
     <div class="row custom_profile_row">
       <div class="col-lg-4 order-lg-1 text-center">
           <img src="<?php echo base_url(); ?>assets/images/girl.jpg" class="mx-auto img-fluid img-circle d-block img_profile" alt="avatar">
           <label class="custom-file">
               <input type="file" id="file" class="custom-file-input">
               <span class="custom-file-control">Upload a profile photo</span>
           </label>
       </div>
         <div class="col-lg-8 order-lg-2">
             <ul class="nav nav-tabs">
                 <li class="nav-item nav_item_profile">
                     <a href="" data-target="#profile" data-toggle="tab" class="nav-link active">Bio</a>
                 </li>
             </ul>
             <div class="tab-content py-4">
               <h3 class="mb-3 profile_h3"><?php echo $girl->username;?></h5>
                                       <?php echo form_open("girls/update_bio/".$girl->slug); ?>
                                       <input type="hidden" name="id"  value="<?php echo $girl->user_id; ?>">
                                       <input type="hidden" name="username"  value="<?php echo $girl->username; ?>">
                                       <div class="form-group region_part">
                                         <label class="region_label_profile" for="region_id">Regions:</label>
                                         <select class="form-control bio_form_profile pull-left" id="sel1"  name="region_id">
                                           <?php foreach($regions as $region) : ?>
                                           <option class="pull-left" value="<?php echo $region['region_id']; ?>"><?php echo $region['name']; ?></option>
                                           <?php endforeach; ?>
                                         </select>
                                       </div>
                        <div class="form-group">
                          <label class="pull-left" for="exampleInputEmail1">Age: </label>
                          <input class="pull-left" type="text" class="form-control" name="age" id="exampleInputEmail1" aria-describedby="emailHelp" value="<?php echo $girl->user_id; ?>">
                        </div>
                        <div class="form-group">
                          <label class="pull-left" for="exampleInputEmail1">Height: </label>
                          <input class="pull-left" type="text" class="form-control" name="height" id="exampleInputEmail1" aria-describedby="emailHelp" value="<?php echo $girl->height; ?>">
                        </div>
                        <div class="form-group">
                          <label class="pull-left" for="exampleInputEmail1">Weight: </label>
                          <input class="pull-left" type="text" class="form-control" name="weight" id="exampleInputEmail1" aria-describedby="emailHelp" value="<?php echo $girl->weight; ?>">
                        </div>
                        <div class="form-group">
                          <label class="pull-left" for="exampleInputEmail1">Orientation: </label>
                          <input class="pull-left" type="email" class="form-control" id="exampleInputEmail1" aria-describedby="emailHelp" placeholder="Enter email">
                        </div>
                        <div class="form-group">
                          <label class="pull-left" for="exampleInputEmail1">Service for: </label>
                          <input class="pull-left" type="email" class="form-control" id="exampleInputEmail1" aria-describedby="emailHelp" placeholder="Enter email">
                        </div>
                        <div class="form-group">
                          <label class="pull-left" for="exampleInputEmail1">Type: </label>
                          <input class="pull-left" type="email" class="form-control" id="exampleInputEmail1" aria-describedby="emailHelp" placeholder="Enter email">
                        </div>
                        <button type="submit" class="btn btn-primary">Submit</button>
                        </form>
             </div>
         </div>
     </div>
    </div>

    <!-- /.container -->
