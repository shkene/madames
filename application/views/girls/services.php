
    <!-- Page Content -->
    <div class="container container-holder">
     <div class="row custom_profile_row">
       <div class="col-lg-4 order-lg-1 text-center">
           <img src="<?php echo base_url(); ?>assets/images/girl.jpg" class="mx-auto img-fluid img-circle d-block img_profile" alt="avatar">
           <label class="custom-file">
               <input type="file" id="file" class="custom-file-input">
               <span class="custom-file-control">Upload a profile photo</span>
           </label>
       </div>
         <div class="col-lg-8 order-lg-2">
             <ul class="nav nav-tabs">
                 <li class="nav-item nav_item_profile">
                     <a href="" data-target="#messages" data-toggle="tab" class="nav-link active">Services</a>
                 </li>
             </ul>
             <div class="tab-content py-4">
               <h3 class="mb-3 profile_h3"><?php echo $girl->username;?></h5>
                   Check on services: <br/>
                   <?php echo form_open("girls/update_services/".$girl->slug); ?>
                   <?php foreach($services as $service) : ?>
                    <input type="checkbox" name="service_id[]" value="<?php echo $service['service_id']; ?>"><?php echo $service['service_name']; ?><br>
                    <input type="hidden" name="id"  value="<?php echo $girl->user_id; ?>">
                    <input type="hidden" name="username"  value="<?php echo $girl->username; ?>">
                   <?php endforeach; ?>
                   <button type="submit" class="btn btn-primary">Submit</button>
                   <?php echo form_close(); ?>
         </div>
     </div>
    </div>
 </div>
    <!-- /.container -->
