
    <!-- Page Content -->
    <?php $this->session->set_userdata('referred_from', current_url()); ?>
    <div class="container container-holder">
     <div class="row custom_profile_row">
       <div class="col-lg-4 order-lg-1 text-center">
           <img src="<?php echo base_url(); ?>assets/images/girl.jpg" class="mx-auto img-fluid img-circle d-block img_profile" alt="avatar">
           <label class="custom-file">
               <input type="file" id="file" class="custom-file-input">
               <span class="custom-file-control">Upload a profile photo</span>
           </label>
           <div class="row">
               <div class="profile_description">
                  <p>Contrary to popular belief, Lorem Ipsum is not simply random text.
                  It has roots in a piece of classical Latin literature from 45 BC, making it over 2000 years old. Richard McClintock,
                  a Latin professor at Hampden-Sydney College in Virginia, looked up one of the more obscure Latin words, consectetur,
                  from a Lorem Ipsum passage.
                </p>
               </div>
           </div>
       </div>
         <div class="col-lg-8 order-lg-2">
             <ul class="nav nav-tabs">
                 <li class="nav-item nav_item_profile">
                     <a href="" data-target="#profile" data-toggle="tab" class="nav-link active">Bio</a>
                 </li>
                 <li class="nav-item nav_item_profile">
                     <a href="" data-target="#messages" data-toggle="tab" class="nav-link">Services</a>
                 </li>
                 <li class="nav-item nav_item_profile">
                     <a href="" data-target="#edit" data-toggle="tab" class="nav-link">Price</a>
                 </li>
                 <li class="nav-item nav_item_profile">
                     <a href="" data-target="#contact" data-toggle="tab" class="nav-link">Contact</a>
                 </li>
                 <li class="nav-item nav_item_profile">
                     <a href="" data-target="#gallery" data-toggle="tab" class="nav-link">Gallery</a>
                 </li>
             </ul>
             <div class="tab-content py-4">
                 <div class="tab-pane active" id="profile">
                     <h3 class="mb-3 profile_h3"><?php echo $girl->username;?></h3>
                     <div class="row">
                         <div class="col-md-6">
                             <p><b class="profile_row">City</b> <?php echo $girl->name; ?></p>
                             <p><b class="profile_row">Age</b> <?php echo $girl->age; ?></p>
                             <p><b class="profile_row">Height</b> <?php echo $girl->height; ?></p>
                             <p><b class="profile_row">Weight</b> <?php echo $girl->weight; ?></p>
                             <p><b class="profile_row">Orientation</b> Straight</p>
                             <p><b class="profile_row">Service for</b> Man, Couple</p>
                             <p><b class="profile_row">Type</b> Latina</p>
                             <?php if($this->session->userdata('username') == $girl->username) : ?>
                             <a class="btn btn-primary bio_button" href="<?php echo base_url(); ?>girls/bio/<?php echo $girl->slug;?>">Edit your bio <img class="profile_img_edit" src="<?php echo base_url(); ?>assets/images/siva.png" alt=""></a>
                             <?php endif; ?>
                         </div>

                     </div>
                     <!--/row-->
                 </div>
                 <div class="tab-pane" id="messages">
                   <h3 class="mb-3 profile_h3"><?php echo $girl->username;?></h3>
                   <div class="row">
                     <div class="col-md-6">
                         <?php foreach($services as $service) : ?>
                       <p><img class="checked_img" src="<?php echo base_url(); ?>assets/images/checked.png" alt=""><?php echo $service['service_name'] ?></p>
                       <?php endforeach; ?>
                       <?php if($this->session->userdata('username') == $girl->username) : ?>
                   <a class="btn btn-primary bio_button" href="<?php echo base_url(); ?>girls/services/<?php echo $girl->slug;?>">Edit your services <img class="profile_img_edit" src="<?php echo base_url(); ?>assets/images/siva.png" alt=""></a>
                   <?php endif; ?>
                   </div>
                 </div>
                 <!--/row-->
                 </div>
                 <div class="tab-pane" id="edit">
                   <h3 class="mb-3 profile_h3"><?php echo $girl->username;?></h3>
                   <div class="row">
                     <div class="col-md-6">
                    <p><b class="profile_row">1 hour</b>  200 CHF</p>
                    <p><b class="profile_row">2 hour</b>  250 CHF</p>
                    <p><b class="profile_row">3 hour</b>  450 CHF</p>
                    <p><b class="profile_row">Night</b>  1000 CHF</p>
                  </div>
                </div>
                 </div>
                 <div class="tab-pane" id="contact">
                   <h3 class="mb-3 profile_h3"><?php echo $girl->username;?></h3>
                   <div class="row">
                     <div class="col-md-6">
                    <p><b class="profile_row">City</b> <?php echo $girl->name; ?></p>
                    <p><b class="profile_row">Phone</b>  <?php echo $girl->contact; ?></p>
                    <p><b class="profile_row">Address</b> /</p>
                    <p><b class="profile_row">Web</b>  /</p>
                  </div>
                </div>
                 </div>
                 <div class="tab-pane" id="gallery">
                   <div class="row gallery-container">
                     <div class="tz-gallery">
                       <div class="row">
                     <div class="col-sm-6 col-md-4 gallery_pic">
                         <a class="lightbox" href="<?php echo base_url(); ?>assets/images/girl.jpg" >
                             <img class="gallery_size" src="<?php echo base_url(); ?>assets/images/girl.jpg" alt="">
                         </a>
                     </div>
            <div class="col-sm-6 col-md-4 gallery_pic">
                <a class="lightbox" href="<?php echo base_url(); ?>assets/images/girl.jpg" >
                    <img class="gallery_size" src="<?php echo base_url(); ?>assets/images/girl.jpg" alt="">
                </a>
            </div>
            <div class="col-sm-12 col-md-4 gallery_pic">
                <a class="lightbox" href="<?php echo base_url(); ?>assets/images/girl.jpg" >
                    <img class="gallery_size" src="<?php echo base_url(); ?>assets/images/girl.jpg" alt="">
                </a>
            </div>
            <div class="col-sm-6 col-md-4 gallery_pic">
                <a class="lightbox" href="<?php echo base_url(); ?>assets/images/girl.jpg" >
                    <img class="gallery_size" src="<?php echo base_url(); ?>assets/images/girl.jpg" alt="">
                </a>
            </div>
          </div>
        </div>
      </div>
    </div>
             </div>
         </div>
     </div>
    </div>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/baguettebox.js/1.8.1/baguetteBox.min.js"></script>
<script>
    baguetteBox.run('.tz-gallery');
</script>

    <!-- /.container -->
