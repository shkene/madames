<!-- Page Content -->
<div class="container container-holder">
  <?php echo validation_errors(); ?>
  <div class="login-form">
    <?php
     echo form_open_multipart('girls/register');
     ?>
        <h2 class="text-center">Sign up</h2>
        <div class="form-group">
        	<div class="input-group">
                <span class="input-group-addon"><i class="fa fa-user"></i></span>
                <input type="text" class="form-control" placeholder="Username" name="username" required="required">
            </div>
        </div>
		      <div class="form-group">
            <div class="input-group">
                <span class="input-group-addon"><i class="fa fa-lock"></i></span>
                <input type="password" class="form-control" placeholder="Password" name="password" required="required">
            </div>
        </div>
        <div class="form-group">
                <div class="input-group">
                    <span class="input-group-addon"><i class="fa fa-lock"></i></span>
                    <input type="password" class="form-control" placeholder="Confirm password" name="passwordtwo" required="required">
                </div>
            </div>
            <div class="form-group">
                    <div class="input-group">
                        <span class="input-group-addon"><i class="fa fa-lock"></i></span>
                        <input type="text" class="form-control" placeholder="Email" name="email" required="required">
                    </div>
                </div>
                <div class="form-group">
                  <label for="region_id">Regions:</label>
                  <select class="form-control" id="sel1"  name="region_id">
                    <?php foreach($regions as $region) : ?>
                    <option value="<?php echo $region['region_id']; ?>"><?php echo $region['name']; ?></option>
                    <?php endforeach; ?>
                  </select>
                </div>
                <div class="form-group">
                  <label>Upload profile picture: </label>
                  <input type="file" name="userfile" size="20">
                </div>
        <div class="form-group">
            <button type="submit" class="btn btn-primary btn-block">Sign up</button>
        </div>
    <?php echo form_close(); ?>
</div>
</div>
