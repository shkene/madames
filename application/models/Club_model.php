<?php
    class Club_model extends CI_Model{
      public function __construct(){
        $this->load->database();
      }
      public function get_clubs(){
         $this->db->join('regions','regions.region_id = clubs.region_id');
         $query = $this->db->get('clubs');
         return $query->result_array();
      }
      public function get_club_by_slug($club_slug){
         $query = $this->db->where('club_slug',$club_slug)->get('clubs');
         return $query->row();
      }
      public function create_club(){
        $slug = url_title($this->input->post('club_name'));
        $data = array(
          'club_name' => $this->input->post('username'),
          'slug' => $slug,
          'region_id' => $this->input->post('region_id'),
          'email' => $this->input->post('email'),
          'password' => $this->input->post('password')
        );
        return $this->db->insert('clubs', $data);
      }
      public function get_regions(){
        $this->db->order_by('name');
        $query = $this->db->get('regions');
        return $query->result_array();
      }

      public function get_clubs_by_region($region_id){
        $this->db->order_by('clubs.id','DESC');
        $this->db->join('regions','regions.region_id = clubs.region_id');
        $query = $this->db->get_where('clubs', array('region_id' => $region_id));
        return $query->result_array();
      }

      public function get_region($id){
        $query = $this->db->get_where('regions',array('region_id'=>$id));
        return $query->row();
      }
    }




 ?>
