<?php
    class Girl_model extends CI_Model{
      public function __construct(){
        $this->load->database();
      }
      public function get_girls(){
         $this->db->join('regions','regions.region_id = users.region_id');
         $query = $this->db->get('users');
         return $query->result_array();
      }
      public function get_new_girls(){
         $this->db->join('regions','regions.region_id = users.region_id');
         $this->db->from('users');
         $this->db->order_by("register_date", "desc");
        $query = $this->db->get();
         return $query->result_array();
      }
      public function get_girl_by_slug($slug){
         $this->db->join('regions','regions.region_id = users.region_id');
         $query = $this->db->where('slug',$slug)->get('users');
         return $query->row();
      }
      public function get_logged_girl($user_id){
        $query = $this->db->where('user_id', $user_id)->get('users');
        return $query->row();
      }
      public function register($profile_photo){
        $slug = url_title($this->input->post('username'));
        $data = array(
          'username' => $this->input->post('username'),
          'slug' => $slug,
          'region_id' => $this->input->post('region_id'),
          'email' => $this->input->post('email'),
          'password' => $this->input->post('password'),
          'profile_photo' => $profile_photo
        );
        return $this->db->insert('users', $data);
      }
      public function login($username, $password){
        //validate
        $this->db->where('username',$username);
        $this->db->where('password',$password);

        $result = $this->db->get('users');
        if($result->num_rows() == 1){
            return $result->row(0)->user_id;
        }else{
          return false;
        }
      }
      public function get_regions(){
        $this->db->order_by('name');
        $query = $this->db->get('regions');
        return $query->result_array();
      }

      public function get_girls_by_region($region_id){
        $this->db->order_by('users.user_id','DESC');
        $this->db->join('regions','regions.region_id = users.region_id');
        $query = $this->db->get_where('users', array('region_id' => $region_id));
        return $query->result_array();
      }

      public function get_new_girls_by_region($region_id){
        $this->db->order_by('users.id','DESC');
        $this->db->join('regions','regions.region_id = users.region_id');
        $query = $this->db->get_where('users', array('region_id' => $region_id));
        return $query->result_array();
      }

      public function get_region($id){
        $query = $this->db->get_where('regions',array('id'=>$id));
        return $query->row();
      }
      public function check_username_exists($username){
        $query = $this->db->get_where('users', array('username' => $username));
        if(empty($query->row_array())){
          return true;
        } else {
          return false;
        }
      }
      public function check_email_exists($email){
        $query = $this->db->get_where('users', array('email' => $email));
        if(empty($query->row_array())){
          return true;
        } else {
          return false;
        }
      }
      public function update_bio(){
         $data = array(
           'region_id' => $this->input->post('region_id'),
           'slug' => $this->input->post('username'),
           'age' => $this->input->post('age'),
           'height' => $this->input->post('height'),
           'weight' => $this->input->post('weight'),
         );
         $this->db->where('user_id',$this->input->post('id'));
         return $this->db->update('users',$data);
      }
      public function get_services_for_girl($slug){
        $this->db->join('users','users.user_id = users_to_services.user_id');
        $this->db->join('services','services.service_id = users_to_services.service_id');
      $query = $this->db->get_where('users_to_services', array('user_slug' => $slug));
        return $query->result_array();
      }
      public function get_services(){
        $query = $this->db->get('services');
        return $query->result_array();
      }
      public function update_service(){
        $service_ids = $this->input->post('service_id');
        $user_id = $this->input->post('id');
        $user_slug = $this->input->post('username');
        $message_data = array();

        foreach($service_ids as $service_id){
        $data = [
          'user_id' =>   $user_id,
          'service_id' => $service_id,
          'user_slug' => $user_slug
        ];
        $message_data[] = $data;

       }
       return $this->db->insert_Batch('users_to_services',$message_data);
      }
    }




 ?>
